# base alpine os
FROM alpine as os-stage

# Install dependencies
# dependencies - node.js,npm,bash,curl,pm2
RUN apk update && apk upgrade && apk add nodejs && apk add npm && apk add bash && apk add curl && npm install pm2@latest -g

WORKDIR /usr/util/

# Install wait-for-it from Git Repo
ADD https://github.com/vishnubob/wait-for-it/archive/master.zip .
RUN unzip master.zip && mv ./wait-for-it-master/wait-for-it.sh ./ && rm -rf ./wait-for-it-master && rm ./master.zip

## Build the custom image by running this command.
## docker build . -f base.Dockerfile --tag base-image 