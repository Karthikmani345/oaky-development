import { RepositoryBase } from "../core/RepositoryBase";

import { dealModel, IDealDocument } from "../models/DealModel";
import { IDeal } from "../interface/IDeal";

export interface IDealRepository extends RepositoryBase<IDealDocument> {
  findById(id: string): Promise<IDealDocument>;
  find(): Promise<IDealDocument[]>;
  save(model: IDeal): Promise<IDealDocument>;
  update(id: string, model: IDeal): Promise<IDealDocument>;
}

export class DealRepository
  extends RepositoryBase<IDealDocument>
  implements IDealRepository
{
  constructor() {
    super(dealModel);
  }

  async findById(id: string): Promise<IDealDocument> {
    return this._model.findById(id);
  }

  async find(): Promise<IDealDocument[]> {
    return this._model.find();
  }

  async save(model: IDeal): Promise<IDealDocument> {
    return this._model.create(model);
  }

  async update(id: string, model: IDeal): Promise<IDealDocument> {
    return this._model.findByIdAndUpdate(id, model);
  }
}
