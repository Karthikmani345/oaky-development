import { RepositoryBase } from "../core/RepositoryBase";

import {
  dealRequestModel,
  IDealRequestDocument,
} from "../models/DealRequestModel";
import { IDealRequest } from "../interface/IDealRequest";

export interface IDealRequestRepository
  extends RepositoryBase<IDealRequestDocument> {
  findById(id: string): Promise<IDealRequestDocument>;
  find(): Promise<IDealRequestDocument[]>;
  save(model: IDealRequest): Promise<IDealRequestDocument>;
  update(id: string, model: IDealRequest): Promise<IDealRequestDocument>;
}

export class DealRequestRepository
  extends RepositoryBase<IDealRequestDocument>
  implements IDealRequestRepository
{
  constructor() {
    super(dealRequestModel);
  }

  async findById(id: string): Promise<IDealRequestDocument> {
    return this._model.findById(id);
  }

  async find(): Promise<IDealRequestDocument[]> {
    return this._model.find();
  }

  async save(model: IDealRequest): Promise<IDealRequestDocument> {
    return this._model.create(model);
  }

  async update(id: string, model: IDealRequest): Promise<IDealRequestDocument> {
    return this._model.findByIdAndUpdate(id, model);
  }
}
