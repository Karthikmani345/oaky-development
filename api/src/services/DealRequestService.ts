import { IDealRequest } from "../interface/IDealRequest";
import {
  IDealRequestRepository,
  DealRequestRepository,
} from "../repository/DealRequestRepository";

export interface IDealRequestService {
  get(id: string): Promise<IDealRequest>;
  getAll(): Promise<IDealRequest[]>;
  create(model: IDealRequest): Promise<IDealRequest>;
  // delete(id: number): Promise<unknown>;
  update(id: string, model: IDealRequest): Promise<unknown>;
}

export class DealRequestService implements IDealRequestService {
  private repository: IDealRequestRepository;

  constructor() {
    this.repository = new DealRequestRepository();
  }

  get(id: string): Promise<IDealRequest> {
    return this.repository.findById(id);
  }

  getAll = async (): Promise<IDealRequest[]> => {
    return this.repository.find();
  };

  create(model: IDealRequest): Promise<IDealRequest> {
    return this.repository.save(model);
  }

  update(id: string, model: IDealRequest): Promise<IDealRequest> {
    return this.repository.update(id, model);
  }
}
