import { IDeal } from "../interface/IDeal";
import { IDealRepository ,DealRepository} from "../repository/DealRepository";

export interface IDealService {
  get(id: string): Promise<IDeal>;
  getAll(): Promise<IDeal[]>;
  create(model: IDeal): Promise<IDeal>;
  // delete(id: number): Promise<unknown>;
  // update(id: number, model: IDeal): Promise<unknown>;
}

export class DealService implements IDealService {
  private repository: IDealRepository;

  constructor() {
    this.repository = new DealRepository();
  }

  get(id: string): Promise<IDeal> {
    return this.repository.findById(id);
  }

  getAll = async (): Promise<IDeal[]> => {
    return this.repository.find();
  };

  create(model: IDeal): Promise<IDeal> {
    return this.repository.save(model);
  }

  update(id: string, model: IDeal): Promise<IDeal> {
    return this.repository.update(id, model);
  }
}
