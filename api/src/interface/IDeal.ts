import IBaseEntity from "./IBaseEntity";

export interface IDeal extends IBaseEntity {
  name: string;
  title: string;
  details: string;
  price: number;
  asset: string;
  isActive: boolean;
}
