import IBaseEntity from "./IBaseEntity";

export interface IHowItWorks extends IBaseEntity {
  name: string;
  isActive: boolean;
}
