import { NextFunction, Request, Response } from "express";

import { IDealRequest } from "../interface/IDealRequest";
import {
  DealRequestService,
  IDealRequestService,
} from "../services/DealRequestService";

export default class DealRequestController {
  private service: IDealRequestService;

  constructor() {
    this.service = new DealRequestService();
  }

  create = async (req: Request, res: Response, next: NextFunction) => {
    try {
      global.logger.info("[DealRequestController.ts] [create] handler");
      const docs = await this.service.create(req.body as IDealRequest);
      global.logger.debug(`[DealRequestController.ts] [create] ${docs}`);
      docs ? res.send(docs) : res.send({});
    } catch (error) {
      next(error);
    }
  };

  update = async (req: Request, res: Response, next: NextFunction) => {
    try {
      global.logger.info("[DealRequestController.ts] [create] handler");
      const id = req?.params?.id as string;
      const model = req.body as IDealRequest;
      const docs = await this.service.update(id, model);
      global.logger.debug(`[DealRequestController.ts] [create] ${docs}`);
      docs ? res.send(docs) : res.send({});
    } catch (error) {
      next(error);
    }
  };

  get = async (req: Request, res: Response, next: NextFunction) => {
    try {
      global.logger.info("[DealRequestController.ts] [get] handler");
      const id = req?.params?.id;
      const docs = await this.service.get(id);
      global.logger.debug(`[DealRequestController.ts] [get] ${docs}`);
      docs ? res.send(docs) : res.send({});
    } catch (error) {
      next(error);
    }
  };

  getAll = async (req: Request, res: Response, next: NextFunction) => {
    try {
      global.logger.info("[DealRequestController.ts] [get] handler");
      const docs = await this.service.getAll();
      global.logger.debug(`[DealRequestController.ts] [get] ${docs}`);
      docs ? res.send(docs) : res.send({});
    } catch (error) {
      next(error);
    }
  };
}
