import { NextFunction, Request, Response } from "express";

import { IDeal } from "../interface/IDeal";
import { DealService } from "../services/DealService";

export default class DealController {
  private service: DealService;

  constructor() {
    this.service = new DealService();
  }

  create = async (req: Request, res: Response, next: NextFunction) => {
    try {
      global.logger.info("[DealController.ts] [create] handler");
      const docs = await this.service.create(req.body as IDeal);
      global.logger.debug(`[DealController.ts] [create] ${docs}`);
      docs ? res.send(docs) : res.send({});
    } catch (error) {
      next(error);
    }
  };

  update = async (req: Request, res: Response, next: NextFunction) => {
    try {
      global.logger.info("[DealController.ts] [create] handler");
      const id = req?.params?.id as string;
      const model = req.body as IDeal;
      const docs = await this.service.update(id, model);
      global.logger.debug(`[DealController.ts] [create] ${docs}`);
      docs ? res.send(docs) : res.send({});
    } catch (error) {
      next(error);
    }
  };

  get = async (req: Request, res: Response, next: NextFunction) => {
    try {
      global.logger.info("[DealController.ts] [get] handler");
      const id = req?.params?.id;
      const docs = await this.service.get(id);
      global.logger.debug(`[DealController.ts] [get] ${docs}`);
      docs ? res.send(docs) : res.send({});
    } catch (error) {
      next(error);
    }
  };

  getAll = async (req: Request, res: Response, next: NextFunction) => {
    try {
      global.logger.info("[DealController.ts] [get] handler");
      const docs = await this.service.getAll();
      global.logger.debug(`[DealController.ts] [get] ${docs}`);
      docs ? res.send(docs) : res.send({});
    } catch (error) {
      next(error);
    }
  };
}
