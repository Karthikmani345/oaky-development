import mongoose, { Schema, Document } from "mongoose";

import { IHowItWorks } from "../interface/IHowItWorks";

const schema = new Schema({
  name: String,
  isActive: {
    type: Boolean,
    default: false,
  },
});

export type IHowItWorksDocument = Document & IHowItWorks;
export const howItWorksModel = mongoose.model<IHowItWorksDocument>(
  "howitworks",
  schema
);
