import mongoose, { Schema, Document } from "mongoose";

import { IDealRequest } from "../interface/IDealRequest";

const schema = new Schema({
  dealId: Schema.Types.ObjectId,
  fromDate: String,
  fromTime: String,
  toDate: String,
  toTime: String,
  amount: Number,
  roomNumber: Number,
  isActive: {
    type: Boolean,
    default: false,
  },
});

export type IDealRequestDocument = Document & IDealRequest;
export const dealRequestModel = mongoose.model<IDealRequestDocument>(
  "dealrequests",
  schema
);
