import { userModel } from "./UserModel";
import { dealModel } from "./DealModel";
import { howItWorksModel } from "./HowItWorksModel";
import { dealRequestModel } from "./DealRequestModel";

const initializeCollection = () => {
  global.logger.info("####### Creating Collection#########");
  dealModel.createCollection();
  dealRequestModel.createCollection();
};

export default initializeCollection;
