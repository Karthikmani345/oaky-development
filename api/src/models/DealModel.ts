import mongoose, { Schema, Document } from "mongoose";

import { IDeal } from "../interface/IDeal";

const schema = new Schema({
  name: String,
  title: String,
  details: String,
  price: Number,
  asset: String,
  isActive: {
    type: Boolean,
    default: false,
  },
});

export type IDealDocument = Document & IDeal;
export const dealModel = mongoose.model<IDealDocument>("deals", schema);
