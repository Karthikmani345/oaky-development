import { Router } from "express";

import UserController from "./controller/UserController";
import DealController from "./controller/DealController";
import DealRequestController from "./controller/DealRequestController";

export const route = Router();

const userController = new UserController();

const dealController = new DealController();
const dealRequestController = new DealRequestController();

route.post("/login", userController.login);
route.post("/register", userController.register);

route.get("/deal/:id", dealController.get);
route.get("/deal", dealController.getAll);
route.post("/deal", dealController.create);
route.put("/deal/:id", dealController.update);

route.get("/request/:id", dealRequestController.get);
route.get("/request", dealRequestController.getAll);
route.post("/request", dealRequestController.create);
route.put("/request/:id", dealRequestController.update);
