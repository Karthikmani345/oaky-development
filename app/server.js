const { createServer } = require('http')
const next = require('next')
const path = require('path');

const pagesDir = path.join(__dirname, 'src', 'app');
console.log('pagesDir : ', pagesDir);

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({
    dev,
    dir: pagesDir
})
const handle = app.getRequestHandler()

app.prepare().then(() => {
    createServer((req, res) => {
        const parsedUrl = new URL(req.url, 'http://w.w')
        const { pathname, query } = parsedUrl

        if (pathname === '/a') {
            app.render(req, res, '/a', query)
        } else if (pathname === '/b') {
            app.render(req, res, '/b', query)
        }
        else if (pathname === '/') {
            app.render(req, res, `/`, query)
        }
        else if (pathname === '/OurTeam') {
            app.render(req, res, '/OurTeam', query)
        }
        else {
            handle(req, res, parsedUrl)
        }
    }).listen(port, (err) => {
        if (err) throw err
        console.log(`> Ready on http://localhost:${port}`)
    })
})