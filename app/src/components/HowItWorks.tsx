export default function HowItWorks() {
  return (
    <>
      <div className="how-it-work">
        <h6>How it works</h6>
        <ul>
          <li>
            <i className="fa fa-check-circle" aria-hidden="true"></i>
            <span> Choose and book your deal or experience</span>
          </li>
          <li>
            <i className="fa fa-check-circle" aria-hidden="true"></i>
            <span> Our team will send you a confirmation e-mail</span>
          </li>
          <li>
            <i className="fa fa-check-circle" aria-hidden="true"></i>
            <span> The fee gets added to your bill</span>
          </li>
        </ul>
      </div>
    </>
  );
}
