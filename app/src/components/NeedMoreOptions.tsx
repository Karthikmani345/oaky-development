import { useEffect, useState } from "react";

import { getDeals } from "../api/api";
import { apiHost } from "../constants/UriConstant";
import { IDealViewModel } from "../interface/IDeal";

type props = {
  id?: string;
};

const NeedMoreOptions: React.FunctionComponent<props> = ({ id }) => {
  const [deals, setDeals] = useState<IDealViewModel[]>([]);

  useEffect(() => {
    getDeals()
      .then((res) => {
        let data = res?.data as unknown as IDealViewModel[];
        // filter the data i.e remove the current selected deal from from the list
        data = data.filter((deal) => deal._id !== id);
        setDeals(data);
      })
      .catch((err) => console.log("err", err));
  }, []);

  return (
    <>
      <section className="slider">
        <div className="container">
          <h4>need More options ?</h4>
          <p>here are deals other guests looked at</p>
          {deals && deals.length > 0 ? (
            <div className="slider-wrap">
              {deals.map((deal) => {
                return (
                  <div className="single-slider">
                    <div className="silder-img">
                      <a href="#">
                        <img src={apiHost + deal.asset} />
                      </a>
                      <a href="#" className="icon">
                        <span>Top deal</span>
                      </a>
                    </div>
                    <div className="slider-content">
                      <h6>{deal.name}</h6>
                      <div className="price">${deal.price}</div>
                    </div>
                  </div>
                );
              })}
            </div>
          ) : null}
        </div>
      </section>
    </>
  );
};
export default NeedMoreOptions;
