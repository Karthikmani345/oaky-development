import * as React from "react";
import Link from "next/link";

interface IAppLinkProps {
  [key: string]: any;
  to:
    | string
    | {
        pathname: string;
        query: { [key: string]: string };
      };
}

const AppLink: React.FunctionComponent<IAppLinkProps> = ({
  to,
  children,
  ...props
}) => {
  return <Link href={to}>{children ? children : null}</Link>;
};

export default AppLink;
