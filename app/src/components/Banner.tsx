import { useEffect } from "react";

export default function Banner() {
  return (
    <>
      <section className="home-banner">
        <div className="container">
          <div className="banner-img">
            <div className="banner-content">
              <h1>Welcome</h1>
              <p>
                Hope you have a good day. Here are some deals to improve your
                stay at the Good Hotel.
              </p>
              <button className="button">get Some discount</button>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
