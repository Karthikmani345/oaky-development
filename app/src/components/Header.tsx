export default function Header() {
  return (
    <>
      <header>
        <nav className="navbar mob-header">
          <div className="logo-img">
            <img src="/oaky_logo.png" alt="logo" />
          </div>
          <div className="menu-list">
            <ul>
              <li>
                <a href="#">
                  <img src="/earth.png" />
                  <img src="/down_arrow.png" />
                </a>
              </li>
              <li className="harmburger">
                <a href="#">
                  <i className="fa fa-bars" aria-hidden="true"></i>
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
    </>
  );
}
