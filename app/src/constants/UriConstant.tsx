export const apiHost = `${process.env.NEXT_PUBLIC_BASE_URI}`;
export const baseUri = `${apiHost}/api/v1`;

export const dealUri = "/deal";
export const requestUri = "/request";
