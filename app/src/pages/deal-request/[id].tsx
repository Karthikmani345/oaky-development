import { GetServerSideProps, NextPage } from "next";
import { useRouter } from "next/router";
import Head from "next/head";
import { useEffect, useState } from "react";
import "react-datetime/css/react-datetime.css";
import Datetime from "react-datetime";

import { getDeal, requestDeal } from "../../api/api";
import { apiHost } from "../../constants/UriConstant";
import { IDealViewModel } from "../../interface/IDeal";
import { IDealRequest } from "../../interface/IDealRequest";

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  try {
    const { id } = ctx.query;
    const res = await getDeal(id as string);
    const data = res?.data;
    console.log("res", data);
    if (!data) {
      return {
        notFound: true,
      };
    }

    return {
      props: {
        data,
      },
    };
  } catch (error) {
    console.log("error", error);
    return {
      notFound: true,
    };
  }
};

type Props = {
  data: IDealViewModel;
};

const DealRequest: NextPage<Props> = ({ data: deal }) => {
  const [fromDate, setFromDate] = useState<string>("");
  const [fromTime, setFromTime] = useState<string>("");
  const [toDate, setToDate] = useState<string>("");
  const [toTime, setToTime] = useState<string>("");
  const [amount, setAmount] = useState<number>(0);
  const [roomNumber, setRoomNumber] = useState<number>(0);
  const [dealId, setDealId] = useState<string>("");
  const router = useRouter();

  useEffect(() => {
    const { id } = router.query;
    setDealId(id as string);
    setRoomNumber(Math.floor(1000 + Math.random() * 9000));
    setAmount(1);
  }, []);

  const onSubmit = (event) => {
    const model = {
      fromDate,
      fromTime,
      toDate,
      toTime,
      amount,
      roomNumber,
      dealId,
    };
    console.log("request deal", model);
    requestDeal(model as IDealRequest)
      .then((res) => {
        console.log("res", res);
        if (res.status === 200 && res.data) {
          // on success.
          router.push("/thank-you/");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <>
      <Head>
        <meta property="my-name" content="kartik"></meta>
      </Head>
      <div className="modal-dialog custom-filter" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">
              Request Deal
            </h5>
            <p>
              In order to complete your request we need some additional details.
            </p>
          </div>
          <div className="modal-body">
            <div className="filter-content">
              <div className="deal-box">
                <h6>Selected deal</h6>
                <div className="single-deal">
                  <div className="deal-img">
                    <img src={apiHost + deal.asset} />
                  </div>
                  <div className="deal-content">
                    <h6>{deal.name}</h6>
                    <p>{deal.title}</p>
                  </div>
                </div>
              </div>
              <div className="filter-form">
                <form>
                  <div className="checkin-input">
                    <label>From</label>
                    {/* <input type="text" name="time" /> */}
                    <Datetime
                      closeOnSelect={true}
                      timeFormat={false}
                      onChange={(newDate: any) => {
                        setFromDate(newDate.format("DD-MM-YYYY"));
                      }}
                    />
                  </div>
                  <div className="checkin-input">
                    {/* <input type="text" name="time" /> */}
                    <Datetime
                      closeOnSelect={true}
                      dateFormat={false}
                      onChange={(newTime: any) => {
                        setFromTime(newTime.format("HH-mm-ss a"));
                      }}
                    />
                  </div>
                  <div className="checkin-input">
                    <label>to</label>
                    <Datetime
                      closeOnSelect={true}
                      timeFormat={false}
                      onChange={(newDate: any) => {
                        setToDate(newDate.format("DD-MM-YYYY"));
                      }}
                    />
                  </div>
                  <div className="checkin-input">
                    <Datetime
                      closeOnSelect={true}
                      dateFormat={false}
                      onChange={(newTime: any) => {
                        setToTime(newTime.format("HH-mm-ss a"));
                      }}
                    />
                  </div>
                  <div className="checkin-input">
                    <label>Amount</label>
                    <input
                      type="number"
                      step="1"
                      value={amount}
                      onChange={(e: any) => {
                        setAmount(e.currentTarget.value);
                      }}
                    />
                  </div>
                  <div className="checkin-input">
                    <label>Room number</label>
                    <input type="text" value={roomNumber} readOnly />
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className="modal-footer">
            <button type="button" className="cancel" data-dismiss="modal">
              Cancel
            </button>
            <button type="button" className="continue" onClick={onSubmit}>
              Continue
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default DealRequest;
