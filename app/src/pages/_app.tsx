import { AppInitialProps, AppContext } from "next/app";

import "../styles/globals.scss";
import Header from "../components/Header";

function MyApp({ Component, pageProps, store }) {
  return (
    <>
      <Header />
      <Component {...pageProps} />
    </>
  );
}

MyApp.getInitialProps = async ({
  Component,
  ctx,
}: AppContext): Promise<AppInitialProps> => {
  const pageProps = Component.getInitialProps
    ? await Component.getInitialProps(ctx)
    : {};

  return { pageProps };
};

export default MyApp;
