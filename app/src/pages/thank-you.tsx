import { GetServerSideProps } from "next";
import Head from "next/head";
import React, { useEffect } from "react";
import { useRouter } from "next/router";

import NeedMoreOptions from "../components/NeedMoreOptions";

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  try {
    const data = await (
      await fetch("https://jsonplaceholder.typicode.com/todos/1")
    ).json();

    if (!data) {
      return {
        notFound: true,
      };
    }

    return {
      props: {
        data,
      },
    };
  } catch (error) {
    console.log("error", error);
    return {
      notFound: true,
    };
  }
};

export default function Thankyou({ data }) {
  const router = useRouter();

  return (
    <>
      <Head>
        <meta property="my-name" content="kartik"></meta>
      </Head>
      <section className="thank-you">
        <div className="container">
          <div className="thanks-content">
            <h1>Thank you</h1>
            <p>
              We will get back to your request as soon as possible via e-mail
            </p>
            <button onClick={()=>router.push("/deals")}>back to all the deals</button>
          </div>
        </div>
      </section>
      <hr />
      <NeedMoreOptions />
    </>
  );
}
