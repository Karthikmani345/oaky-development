import { GetServerSideProps, NextPage } from "next";
import Link from "next/link";
import Head from "next/head";
import { useRouter } from "next/router";

import { IDealViewModel } from "../../interface/IDeal";
import { apiHost } from "../../constants/UriConstant";
import HowItWorks from "../../components/HowItWorks";
import NeedMoreOptions from "../../components/NeedMoreOptions";

import { getDeal } from "../../api/api";

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  try {
    const { id } = ctx.query;
    const res = await getDeal(id as string);
    const data = res?.data;
    console.log("res", data);
    if (!data) {
      return {
        notFound: true,
      };
    }

    return {
      props: {
        data,
      },
    };
  } catch (error) {
    console.log("error", error);
    return {
      notFound: true,
    };
  }
};

type Props = {
  data: IDealViewModel;
};

const DealsDetails: NextPage<Props> = ({ data: deal }) => {
  const router = useRouter();

  return (
    <>
      <Head>
        <meta property="my-name" content="kartik"></meta>
      </Head>

      <section className="deatil-page">
        <div className="container">
          <div className="detail-single">
            <div className="detail-img">
              <img src={apiHost + deal.asset} />
              <a href="#" className="icon">
                <span>Top deal</span>
              </a>
            </div>
            <div className="detail-content">
              <h1> {deal.name} </h1>
              <span>{deal.title}</span>
              <p>{deal.details}</p>
            </div>
            <HowItWorks />
            <hr />
          </div>
        </div>
      </section>
      <NeedMoreOptions id={deal._id} />
      <hr />
      <div className="book-button">
        <div className="price-info">
          <div className="price">
            <span>$1234</span>
            {/* <s> $123</s> */}
          </div>
          <p>price per person </p>
        </div>
        <div className="book-btn">
          <button
            className="btn"
            onClick={() => router.push(`/deal-request/${deal._id}`)}
          >
            take a deal
          </button>
        </div>
      </div>
    </>
  );
};

export default DealsDetails;
