import { GetServerSideProps, NextPage } from "next";
import Link from "next/link";
import Head from "next/head";

import Banner from "../components/Banner";
import {  apiHost } from "../constants/UriConstant";
import {  IDealViewModel } from "../interface/IDeal";
import { getDeals } from "../api/api";


export const getServerSideProps: GetServerSideProps = async (ctx) => {
  try {
    const res = await getDeals();
    const data = res?.data;
    console.log("res", data);

    if (!data) {
      return {
        notFound: true,
      };
    }

    return {
      props: {
        data,
      },
    };
  } catch (error) {
    console.log("error", error);
    return {
      notFound: true,
    };
  }
};

type Props = {
  data: IDealViewModel[];
};

const Deals: NextPage<Props> = ({ data }) => {
  return (
    <>
      <Head>
        <meta property="oaky" content="oaky deals"></meta>
      </Head>
      {/* **** Home Section **** */}
      <Banner />
      <section className="listing-content">
        <div className="container">
          <ul className="tab-list">
            <li className="active">
              <a href="#">Featured</a>
            </li>
            <li>
              <a href="#">Deals</a>
            </li>
            <li>
              <a href="#">Upgrades</a>
            </li>
          </ul>
          <div className="content-listing">
            <h6>Recommended for you </h6>
            <p>Handpicked experiences and deals</p>
          </div>

          {data && data.length > 0 ? (
            <div className="listing-grid">
              {data.map((deal) => {
                return (
                  <div className="single-content">
                    <div className="listing-img">
                      <Link href={`/deal-details/${deal._id}`}>
                        <img
                          src={apiHost + deal.asset}
                          style={{ height: "100%", width: "100%" }}
                        />
                      </Link>
                    </div>
                    <div className="listing-content">
                      <h6>{deal.name}</h6>
                      <p>{deal.details}</p>
                      <div className="price-deatils">
                        {/* <s> $123</s>
                      <span>$1234</span> */}
                        <span>${deal.price}</span>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          ) : null}
        </div>
      </section>
      {/* END Hom Section */}
    </>
  );
};
export default Deals;
