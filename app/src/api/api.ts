import { baseUri, dealUri, requestUri } from "../constants/UriConstant";
import HttpClient from "../core/HttpClient";
import { IDeal } from "../interface/IDeal";
import { IDealRequest } from "../interface/IDealRequest";

export async function getDeals() {
  try {
    const data = await HttpClient.fetch<IDeal[]>(`${baseUri}/${dealUri}`, {});
    return data;
  } catch (error) {
    throw error;
  }
}

export async function getDeal(id: string) {
  try {
    const data = await HttpClient.fetch<IDeal>(
      `${baseUri}/${dealUri}/${id}`,
      {}
    );
    return data;
  } catch (error) {
    throw error;
  }
}

export async function requestDeal(model: IDealRequest) {
  try {
    const data = await HttpClient.create<IDealRequest>(
      `${baseUri}/${requestUri}`,
      model
    );
    return data;
  } catch (error) {
    throw error;
  }
}
