import IBaseEntity from "./IBaseEntity";

export interface IDeal extends IBaseEntity {
  name: string;
  title: string;
  details: string;
  price: number;
  asset: string;
  isActive: boolean;
}

export interface IDealViewModel extends IDeal {
  _id: string;
}
