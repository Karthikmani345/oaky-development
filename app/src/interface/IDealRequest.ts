import IBaseEntity from "./IBaseEntity";

export interface IDealRequest extends IBaseEntity {
  dealId: string;
  fromDate: string;
  fromTime: string;
  toDate: string;
  toTime: string;
  amount: number;
  roomNumber: number;
  isActive: boolean;
}
